from django.urls import path, include
from .views import SearchProduct

urlpatterns = [
    path('search', SearchProduct.as_view()),
]
