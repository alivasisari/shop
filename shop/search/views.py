from django.db.models import F
from django.http import HttpResponse

from django.views.generic import ListView
from rest_framework.views import APIView

from product.models import StoreProduct, Product


# Create your views here.

class SearchProduct(ListView):
    model = Product
    template_name = "product/products_list.html"
    context_object_name = "products"

    def get_queryset(self):
        query = self.request.GET
        product_list = Product.objects.none()
        if len(query) < 1:
            product_list = Product.objects.all()
        for k, v in self.request.GET.items():
            if k == "s" or k == "q":
                product_list |= StoreProduct.objects.all().filter(product__title__contains=v).values(
                    prouduct_id=F('product__id'), title=F('product__title'), image=F('product__image'))
                product_list = product_list.annotate(id=F('product_id')).values("id", "title", "image")
            elif k == "p" or k == "q":
                product_list |= self.model.objects.all().filter(title__contains=v)
        return product_list
