from urllib.request import urlopen

from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
from django.db import models


# Create your models here.
class Store(models.Model):
    name = models.CharField(max_length=250, verbose_name="Name")
    url = models.URLField(verbose_name="Store url address")
    image = models.ImageField(default=None, blank=True, null=True, verbose_name="Store image",
                              upload_to="images/stores/")

    class Meta:
        verbose_name = "Store"
        verbose_name_plural = "Stores"
        indexes = [
            models.Index(fields=['name', ]),
        ]

    def __str__(self):
        return self.name

    def get_details(self):
        return {"name": self.name, "url": self.url}

    def get_image_from_url(self, url):
        img_tmp = NamedTemporaryFile()
        with urlopen(url) as uo:
            assert uo.status == 200
            img_tmp.write(uo.read())
            img_tmp.flush()
        img = File(img_tmp)
        self.image.save(f"{self.name}_{str(self.id)}.{url.split('.')[-1]}", img)
