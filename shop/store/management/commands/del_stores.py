from django.core.management.base import BaseCommand
from store.models import Store


class Command(BaseCommand):
    help = 'Delete all stores'

    def handle(self, *args, **kwargs):
        Store.objects.all().delete()
