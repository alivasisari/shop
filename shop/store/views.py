from django.http import HttpResponse

# Create your views here.
from django.views.generic import ListView
from rest_framework.views import APIView
from .serializers import StoreSerializer

from .models import Store


class AddStore(APIView):
    def post(self, request):
        storeSerializer = StoreSerializer(data=request.data)
        if storeSerializer.is_valid():
            storeSerializer.save()
            return HttpResponse(status=200)
        return HttpResponse(status=400)


class StoresList(ListView):
    template_name = "store/stores_list.html"
    model = Store
    context_object_name = "stores"
