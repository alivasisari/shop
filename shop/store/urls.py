from django.urls import path, include
from .views import AddStore, StoresList

urlpatterns = [
    path('api/stores/create/', AddStore.as_view()),
    path('stores/', StoresList.as_view(), name="stores"),
]
