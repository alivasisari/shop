from rest_framework import serializers

from store.models import Store


class StoreSerializer(serializers.ModelSerializer):
    image_url = serializers.URLField(write_only=True)

    class Meta:
        model = Store
        fields = "__all__"
        extra_kwargs = {
            'image': {
                'required': False,
            },
        }

    def create(self, validated_data):
        store_search = Store.objects.filter(name=validated_data["name"]).first()
        store = store_search
        image_url = validated_data.pop("image_url")
        if store_search is None:
            store = Store.objects.create(**validated_data)
            if image_url is not None:
                store.get_image_from_url(image_url)
        return store
