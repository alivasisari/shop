# Generated by Django 4.0.6 on 2022-09-07 18:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('store', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=250, verbose_name='Title')),
                ('available', models.BooleanField(default=False, verbose_name='Is Available')),
                ('image', models.ImageField(blank=True, default=None, null=True, upload_to='images/products/', verbose_name='Product image')),
            ],
            options={
                'verbose_name': 'Product',
                'verbose_name_plural': 'Products',
            },
        ),
        migrations.CreateModel(
            name='ProductCategory',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250, verbose_name='Name')),
            ],
            options={
                'verbose_name': 'Category',
                'verbose_name_plural': 'Categories',
            },
        ),
        migrations.CreateModel(
            name='StoreProduct',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('url', models.URLField(unique=True, verbose_name='Product URL')),
                ('price', models.IntegerField(default=0, verbose_name='Price')),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='products', to='product.product', verbose_name='Product')),
                ('store', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='stores', to='store.store', verbose_name='Store')),
            ],
            options={
                'verbose_name': 'StoreProduct',
                'verbose_name_plural': 'StoreProduct',
            },
        ),
        migrations.AddIndex(
            model_name='productcategory',
            index=models.Index(fields=['name'], name='product_pro_name_fd0d2e_idx'),
        ),
        migrations.AddField(
            model_name='product',
            name='categories',
            field=models.ManyToManyField(related_name='product_categories', to='product.productcategory', verbose_name='Product Categories'),
        ),
        migrations.AddField(
            model_name='product',
            name='stores',
            field=models.ManyToManyField(related_name='product_stores', to='store.store', verbose_name='Stores'),
        ),
        migrations.AddIndex(
            model_name='product',
            index=models.Index(fields=['title'], name='product_pro_title_07e491_idx'),
        ),
    ]
