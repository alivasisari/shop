from django.shortcuts import render
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import HttpResponse
from .models import Product
from .serializers import ProductSerializer
from django.views.generic import ListView
from django.shortcuts import get_object_or_404


# Create your views here.
class AddProduct(APIView):
    def post(self, request):
        for data in request.data:
            product_serializer = ProductSerializer(data=data)
            if product_serializer.is_valid():
                product_serializer.save()
                return HttpResponse(status=200)
            return HttpResponse(status=400)


class ProductDetails(APIView):
    model = Product
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'product/product.html'

    def get(self, request, product_id):
        print("a")
        product = get_object_or_404(Product, id=product_id)
        product_details = Product.objects.get_product_details(product_id)
        return Response({"details": product_details})


class ProductsList(ListView):
    template_name = "product/products_list.html"
    model = Product
    context_object_name = "products"
