from django.urls import path, include
from .views import AddProduct, ProductsList , ProductDetails

urlpatterns = [
    path('api/products/create/', AddProduct.as_view()),
    path('products/', ProductsList.as_view(), name="products"),
    path('products/<int:product_id>', ProductDetails.as_view()),
]
