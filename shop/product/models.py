from django.db import models
from store.models import Store
from urllib.request import urlopen

from django.core.files import File
from django.core.files.temp import NamedTemporaryFile


class ProductManager(models.Manager):
    def get_product_details(self, product_id):
        product = Product.objects.filter(id=product_id).values()[0]
        stores = StoreProduct.objects.filter(product__id=product_id).values()
        stores_list = []
        for store in stores:
            try:
                store_detail = Store.objects.get(id=store["store_id"]).get_details()
                store_detail["product_url"] = store["url"]
                store_detail["product_price"] = store["price"]
                stores_list.append(store_detail)
            except:
                continue

        product["stores"] = stores_list
        print(product)
        return product


class ProductCategory(models.Model):
    name = models.CharField(max_length=250, verbose_name="Name")

    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"
        indexes = [
            models.Index(fields=['name', ]),
        ]

    def __str__(self):
        return self.name


# Create your models here.
class Product(models.Model):
    title = models.CharField(max_length=250, verbose_name="Title")
    stores = models.ManyToManyField(to=Store, verbose_name="Stores", related_name="product_stores")
    categories = models.ManyToManyField(to=ProductCategory, related_name="product_categories",
                                        verbose_name="Product Categories")
    available = models.BooleanField(verbose_name="Is Available", default=False)
    image = models.ImageField(default=None, blank=True, null=True, verbose_name="Product image",
                              upload_to="images/products/")
    objects = ProductManager()

    class Meta:
        verbose_name = "Product"
        verbose_name_plural = "Products"
        indexes = [
            models.Index(fields=['title', ]),
        ]

    def get_image_from_url(self, url):
        img_tmp = NamedTemporaryFile()
        with urlopen(url) as uo:
            assert uo.status == 200
            img_tmp.write(uo.read())
            img_tmp.flush()
        img = File(img_tmp)
        self.image.save(f"{str(self.id)}.{url.split('.')[-1]}", img)

    def __str__(self):
        return self.title


class StoreProduct(models.Model):
    product = models.ForeignKey(to=Product, verbose_name="Product", on_delete=models.CASCADE,
                                related_name="products")
    store = models.ForeignKey(to=Store, verbose_name="Store", on_delete=models.CASCADE, related_name="stores")
    url = models.URLField(unique=True, verbose_name="Product URL")
    price = models.IntegerField(default=0, verbose_name="Price")

    class Meta:
        verbose_name = "StoreProduct"
        verbose_name_plural = "StoreProduct"

    def __str__(self):
        return f"{self.url}"
