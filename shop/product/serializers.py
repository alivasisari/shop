from rest_framework import serializers

from product.models import Product, ProductCategory, StoreProduct
from store.models import Store
from store.serializers import StoreSerializer


class ProductCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductCategory
        fields = "__all__"

    def create(self, validated_data):
        product_category_search = ProductCategory.objects.filter(name=validated_data["name"]).first()
        product_category = product_category_search
        if product_category_search is None:
            product_category = ProductCategory.objects.create(**validated_data)
        return product_category


class ProductSerializer(serializers.ModelSerializer):
    store_product = serializers.DictField(write_only=True)
    image_url = serializers.URLField(write_only=True, allow_null=True)
    category = serializers.DictField(write_only=True)
    store = serializers.DictField(write_only=True)

    class Meta:
        model = Product
        fields = "__all__"
        extra_kwargs = {
            'categories': {
                'required': False,
            },
            'stores': {
                'required': False,
            },
            'image': {
                'required': False,
            },
        }

    def create(self, validated_data):
        print(validated_data)
        store_data = validated_data.pop("store")
        category_data = validated_data.pop("category")
        store_product_data = validated_data.pop("store_product")
        product_data = validated_data

        store_serializer = StoreSerializer(data=store_data)
        category_serializer = ProductCategorySerializer(data=category_data)

        image_url = product_data.pop("image_url")

        product = Product.objects.filter(title=product_data["title"]).first()
        if product is None:
            product = Product.objects.create(**product_data)
        if image_url is not None:
            product.get_image_from_url(image_url)

        category = store = store_product = None
        if store_serializer.is_valid():
            store = store_serializer.save()
        if category_serializer.is_valid():
            category = category_serializer.save()
        if store is not None:
            product.stores.set([store])
        if category is not None:
            product.categories.set([category])

        store_product_data["store"] = store.pk
        store_product_data["product"] = product.pk
        store_product_serializer = StoreProductSerializer(data=store_product_data)
        if store_product_serializer.is_valid():
            store_product = store_product_serializer.save()
            store_product.store = store
            store_product.product = product

        return product


class StoreProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = StoreProduct
        fields = "__all__"
        extra_kwargs = {
            'product': {
                'required': False,
            },
            'store': {
                'required': False,
            },
        }

    def create(self, validated_data):
        store_product_search = StoreProduct.objects.filter(url=validated_data["url"]).first()
        store_product = store_product_search
        if store_product_search is None:
            store_product = StoreProduct.objects.create(**validated_data)

        return store_product
